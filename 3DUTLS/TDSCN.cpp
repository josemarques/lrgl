#include "TDSCN.h"
TDSCN::TDSCN(){

}
void TDSCN::INIT(){
	glEnable(GL_MULTISAMPLE);
// Enable depth test
	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	// Accept fragment if it is closer to the camera than the former one

	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	//glEnable(GL_CULL_FACE);
	//FRAMEBUFFER=FBO(100,100);


	for (int sm=0;sm<SHELLMODELS.size();sm++){
	SHELLMODELS[sm].INIT();

	}

	for (int cm=0;cm<CLOUDMODELS.size();cm++){
		CLOUDMODELS[cm].INIT();
	}

}

void TDSCN::DRW(){

	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for (int sm=0;sm<SHELLMODELS.size();sm++){
		glUseProgram(SHELLMODELS[sm].programID);

		GLuint LightID = glGetUniformLocation(SHELLMODELS[sm].programID, "LightPosition_worldspace");

		GLuint MatrixID = glGetUniformLocation(SHELLMODELS[sm].programID, "MVP");
		GLuint ViewMatrixID = glGetUniformLocation(SHELLMODELS[sm].programID, "V");
		SHELLMODELS[sm].MatrixID=glGetUniformLocation(SHELLMODELS[sm].programID, "M");

		MVP= CAMERAS[ACAM].pM * CAMERAS[ACAM].vM * SHELLMODELS[sm].mM; // Remember, matrix multiplication is the other way around

		glUniformMatrix4fv(SHELLMODELS[sm].MatrixID, 1, GL_FALSE, &SHELLMODELS[sm].mM[0][0]);
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &CAMERAS[ACAM].vM[0][0]);

		glm::vec3 lightPos = glm::vec3(40,40,40);
		glUniform3f(LightID, lightPos.x, lightPos.y, lightPos.z);

		SHELLMODELS[sm].DRW();
	}

	//glDepthMask(false); //disable z-testing
	//glDisable(GL_DEPTH_TEST);

	for (int cm=0;cm<CLOUDMODELS.size();cm++){
		glUseProgram(CLOUDMODELS[cm].programID);

		GLuint MatrixID = glGetUniformLocation(CLOUDMODELS[cm].programID, "MVP");
		MVP= CAMERAS[ACAM].pM * CAMERAS[ACAM].vM * CLOUDMODELS[cm].mM; // Remember, matrix multiplication is the other way around
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		CLOUDMODELS[cm].DRW();

	}
	//glEnable(GL_DEPTH_TEST);
	//glDepthMask(true); //enable z-testing (for the next frame)
	glUseProgram(0);

}

void TDSCN::DEL(){
	for (int sm=0;sm<SHELLMODELS.size();sm++){
		SHELLMODELS[sm].DEL();
	}

	for (int cm=0;cm<CLOUDMODELS.size();cm++){
		CLOUDMODELS[cm].DEL();
	}

}

FBO::FBO(){

}

void FBO::INIT(float width, float height){
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

// FBO& FBO::operator=(const FBO& fboo){//Igualador de clase
// 	//std::cout<<"FFSSET& FFSSET::operator=::0\n";
// 	fbo=fboo.fbo;
// 	texture=fboo.texture;
// 	rbo=fboo.rbo;
// 	return *this;
// }

FBO::~FBO(){
	if(fbo>0){
	glDeleteFramebuffers(1, &fbo);
	glDeleteTextures(1, &texture);
	glDeleteRenderbuffers(1, &rbo);
	}
}

unsigned int FBO::GFBT(){
	return texture;
}

void FBO::RFB(float width, float height){
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0);

	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo);
}

void FBO::Bind() const{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
}

void FBO::Unbind() const{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
