#include "CAM.h"

CAM::CAM() {
	//INIT();
}

void CAM::INIT(int mx, int my, int wx, int wy) {

	pmx=mx;
	pmy=my;
	pwx=wx;
	pwy=wy; //Previous position values

	// CRXM();
	// CRYM();
	// CRZM();
 //
	// CTM();
	// CSM();
 //
	// CPRM();
 //
	// CM();

}



void CAM::UPDT(float view_w, float view_h ,int mx, int my, int wx, int wy, bool rclick, bool mclick,  bool lclick){
	vw=view_w;//Aspect ratio update
	vh=view_h;

	float dx=cpx-tpx;//Diferencial camera position
	float dy=cpy-tpy;
	float dz=cpz-tpz;

	float r, theta, phi;
	CtSC(dx, dy, dz, r, theta, phi);

	//phi+=0.01;

	if (rclick) {//Orbit arround target

		//std::cout<<"CAM::UPDATE::mx,my="<<mx<<","<<my<<std::endl;
		grx -= (pmy - my)*0.005;
		gry -= (pmx -mx)*0.005;

	}
	if (mclick) {
		//std::cout<<"CAM::UPDATE::tx,ty="<<tx<<","<<ty<<std::endl;
		tpx -= (pmx-mx)*0.01;
		tpy -= (pmy-my)*0.01;
	}

	if(wx!=pwx){
		tpz+=(pwx-wx);
		// sx+=(wx-pwx); sy+=(wx-pwx); sz+=(wx-pwx);
		// CSM();
	}
	//CSM();

	//if (thet>2*pi)thet = 0; if (phi>2*pi)phi = 0;

	pmx=mx;
	pmy=my;
	pwx=wx;
	pwy=wy; //Previous position values

	//StCC(dx, dy, dz, r, theta, phi);

	cpx=dx+tpx;//Camera position calculation
	cpy=dy+tpy;
	cpz=dz+tpz;



	pM=glm::perspective(fov, vw/vh, zn, zf);//Proyection matrix calculation // Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units

	vM=glm::lookAt(
		glm::vec3(cpx, cpy, cpz), // Camera is at (4,3,-3), in World Space
		glm::vec3(tpx, tpy, tpz), // and looks at the origin
		glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
	);//View matrix calculation

	vM = glm::rotate(vM, gry, glm::vec3(0, 1, 0));
	vM = glm::rotate(vM, grx, glm::vec3(1, 0, 0));

}


void CtSC(float &x, float &y, float &z, float &r, float &theta, float &phi){//Connvert cartisian to spherical coordinate sistem

    r = sqrt(x * x + y * y + z * z);

    if (r != 0) {
        theta = acos(z / r);
        phi = atan2(y, x);
    }
    else {
        theta = 0;
        phi= 0;
    }

}

void StCC(float &x, float &y, float &z, float &r, float &theta, float &phi){//Connvert spherical to cartesian coordinate sistem
	x=r*sin(theta)*cos(phi);//Cartisian coordinates transform
	y=r*sin(theta)*sin(phi);
	z=r*cos(theta);

}
