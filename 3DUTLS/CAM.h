#ifndef CAM_H_INCLUDED
#define CAM_H_INCLUDED

#include "../SUBM/glad/glad.h"//Glad
#include <GLFW/glfw3.h>

#include <../SUBM/glm/glm/glm.hpp>
#include <../SUBM/glm/glm/gtc/matrix_transform.hpp>

#include "../SUBM/MATH/C++/MTR.h"


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <math.h>

const double pi=3.1415926535897931;

class CAM {
public:
	float pmx=0, pmy=0, pwx=0, pwy=0; //Previous mouse position values
	float mx=0, my=0, wx=0, wy=0; //Actual mouse position values

	float cpx=40, cpy=40, cpz=40;//Camera position values
	float tpx=0, tpy=0, tpz=0;// Target position

	float fov=pi/4;//Field ov fiew
	float vw=160, vh=90;//View wight and view hight
	float zn=10, zf=500;//Znear and z far

	float grx=0;
	float gry=0;
	float grz=0;

	//float vM[16];//View matrix
	//float pM[16];//Proyection matix

	glm::mat4 pM = glm::perspective(fov, vw/vh, zn, zf);//Proyection matrix
	// Camera matrix
	glm::mat4 vM = glm::lookAt(
		glm::vec3(cpx, cpy, cpz), // Camera is at (4,3,-3), in World Space
		glm::vec3(tpx, tpy, tpz), // and looks at the origin
		glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
	);//View matrix

	CAM();
	// ~CAM() {
	// }
	void INIT(int mx, int my, int wx, int wy);

	void UPDT(float view_w, float view_h, int mx, int my, int wx, int wy, bool rclick, bool mclick,  bool lclick);

	//void DRAW(GLuint &shaderProgram , std::string model);

	// void CPPRM(float Zfar=10,float Znear=0.1,float AR=16.0f/9.0f,float FOV=pi*(1/3));//Compute poerspective proyection matrix
	// void COPRM(float left = -1.0f, float right = 1.0f, float bottom = -1.0f, float top = 1.0f, float nearVal = -1.0f, float farVal = 1.0f);//Compute ortographicproyection matrix
 //
	// void CPM();//Compute position/translation matrix
	// void CRXM();//Compute x rotation matrix
	// void CRYM();//Compute y rotation matrix
	// void CRZM();//Compute x rotation matrix
	// void CSM();//Compute scale matrix
	// void CTM();//Compute Transform nmatrix

};

void CtSC(float &x, float &y, float &z, float &r, float &theta, float &phi);//Connvert cartisian to spherical coordinate sistem

void StCC(float &x, float &y, float &z, float &r, float &theta, float &phi);//Connvert spherical to cartesian coordinate sistem


#endif
