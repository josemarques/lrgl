//Tridimensional objects

#include "TDOBJ.h"

void VCLUD::INIT(){
	glEnable(GL_PROGRAM_POINT_SIZE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	//TODO sorting transparent points https://learnopengl.com/Advanced-OpenGL/Blending    https://stackoverflow.com/questions/37780345/opengl-how-to-create-order-independent-transparency

	glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

	//GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, VP.size() * sizeof(float), &VP[0], GL_DYNAMIC_DRAW);


	//GLuint uvbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, VC.size() * sizeof(float), &VC[0], GL_DYNAMIC_DRAW);


	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
	);

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(
		1,                                // attribute
		4,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
	);

	glBindVertexArray(0);

	//std::cout<<"VCLUD::INIT"<<std::endl;

}

void VCLUD::MOD(){//Modify field
	glBindVertexArray(VertexArrayID);

	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, VP.size() * sizeof(float), &VP[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, VC.size() * sizeof(float), &VC[0], GL_DYNAMIC_DRAW);

	glBindVertexArray(0);
}


void VCLUD::DRW(){
	if(RENDR) {

	mM = glm::mat4(1.0f);//TODO

	glBindVertexArray(VertexArrayID);

	//Alpha rendering TODO
	unsigned int ALPHATHRESOLDID = glGetUniformLocation(programID, "aThreshold");
	glUniform1f(ALPHATHRESOLDID, 0);

	//for(float ac=1;ac>0;ac-=0.05){
	//	glUniform1f(ALPHATHRESOLDID, ac);
	//	glDrawArrays(GL_POINTS, 0, VP.size());
	//}

	for(float ac=0;ac<VP.size()*perun;ac+=VP.size()/nzons){//Sorted from more to less opacity
		glDrawArrays(GL_POINTS, ac, ac+(VP.size()/nzons));
	}

	glBindVertexArray(0);
	}
}

void VCLUD::DEL(){
	glDeleteProgram(programID);
}


// VSHEL::~VSHEL(){
// }

void VSHEL::LDGM(std::string modelpath, std::string materialpath){

	vertices.clear();
	uvs.clear();
	normals.clear();
	indices.clear();

	//std::cout<<"VSHEL::LDGM::000"<<std::endl;

	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(modelpath.c_str(), 0/*aiProcess_JoinIdenticalVertices | aiProcess_SortByPType*/);
	if( !scene) {
		fprintf( stderr, importer.GetErrorString());
		getchar();
	}

	//std::cout<<"VSHEL::LDGM::scene->mNumMeshes="<<scene->mNumMeshes<<std::endl;
	 NumMesh=scene->mNumMeshes;
	for(int m=0;m<NumMesh;m++){
		vertices.push_back(std::vector<float>());
		uvs.push_back(std::vector<float>());
		normals.push_back(std::vector<float>());
		indices.push_back(std::vector<unsigned short>());

		aiMesh* mesh = scene->mMeshes[m]; // In this simple example code we always use the 1rst mesh (in OBJ files there is often only one anyway)

		// Fill vertices positions
		//vertices.reserve(mesh->mNumVertices);
		for(unsigned int i=0; i<mesh->mNumVertices; i++){
			aiVector3D pos = mesh->mVertices[i];
			vertices[m].push_back(pos.x);
			vertices[m].push_back(pos.y);
			vertices[m].push_back(pos.z);
		}

		//std::cout<<"VSHEL::LDGM::100"<<std::endl;

		// // Fill vertices texture coordinates
		// uvs.reserve(mesh->mNumVertices);
		// for(unsigned int i=0; i<mesh->mNumVertices; i++){
		// 	aiVector3D UVW = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords; AssImp supports 8 UV sets.
		// 	uvs.push_back(glm::vec2(UVW.x, UVW.y));
		// }

		// Fill vertices normals
		//normals.reserve(mesh->mNumVertices);
		for(unsigned int i=0; i<mesh->mNumVertices; i++){
			aiVector3D n = mesh->mNormals[i];
			normals[m].push_back(n.x);
			normals[m].push_back(n.y);
			normals[m].push_back(n.z);
		}

		//std::cout<<"VSHEL::LDGM::200"<<std::endl;

		// Fill face indices
		//indices.reserve(3*mesh->mNumFaces);
		for (unsigned int i=0; i<mesh->mNumFaces; i++){
			// Assume the model has only triangles.
			indices[m].push_back(mesh->mFaces[i].mIndices[0]);
			indices[m].push_back(mesh->mFaces[i].mIndices[1]);
			indices[m].push_back(mesh->mFaces[i].mIndices[2]);
		}

	//std::cout<<"VSHEL::LDGM::300"<<std::endl;
	}
	// The "scene" pointer will be deleted automatically by "importer"

}//Load geometry

void VSHEL::INIT(){

	vertexbuffer.clear();
	uvbuffer.clear();
	normalbuffer.clear();

	for(int m=0;m<NumMesh;m++){

		vertexbuffer.push_back(0);
		//GLuint vertexbuffer;
		glGenBuffers(1, &vertexbuffer[m]);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[m]);
		glBufferData(GL_ARRAY_BUFFER, vertices[m].size() * sizeof(float), &vertices[m][0], GL_STATIC_DRAW);

		uvbuffer.push_back(0);
		//GLuint uvbuffer;
		glGenBuffers(1, &uvbuffer[m]);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer[m]);
		glBufferData(GL_ARRAY_BUFFER, uvs[m].size() * sizeof(float), &uvs[m][0], GL_STATIC_DRAW);

		normalbuffer.push_back(0);
		//GLuint normalbuffer;
		glGenBuffers(1, &normalbuffer[m]);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer[m]);
		glBufferData(GL_ARRAY_BUFFER, normals[m].size() * sizeof(float), &normals[m][0], GL_STATIC_DRAW);

			// Get a handle for our "LightPosition" uniform
		//glUseProgram(programID);
	}

}
void VSHEL::DRW(){
	if(RENDR){

	ModelMatrixID = glGetUniformLocation(programID, "M");
	mM = glm::mat4(1.0f);//TODO
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &mM[0][0]);

	for(int m=0;m<NumMesh;m++){

		unsigned int difuse_color_ID = glGetUniformLocation(programID, "difuse_color");
		//glm::vec3 difusecolor = glm::vec3(Kdr,Kdg, Kdb);
		glUniform3f(difuse_color_ID, Kdr,Kdg, Kdb);

		unsigned int specular_color_ID = glGetUniformLocation(programID, "specular_color");
		//glm::vec3 specularcolor = glm::vec3(Ksr,Ksg, Ksb);
		glUniform3f(specular_color_ID, Ksr,Ksg, Ksb);
		// glm::mat4 myMatrix = glm::translate(glm::mat4(), glm::vec3(px, py, pz));
		// glm::mat4 myScalingMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(rx,ry,rz));


// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[m]);
		glVertexAttribPointer(
			0,                  // attribute
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, uvbuffer[m]);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, normalbuffer[m]);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);

		// Draw the triangles !
		glDrawArrays(GL_TRIANGLES, 0, vertices[m].size() );

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
	}


	}

}

void VSHEL::DEL(){
	// for(int m=0;m<NumMesh;m++){
	// 	glDeleteBuffers(1, &vertexbuffer[m]);
	// 	glDeleteBuffers(1, &uvbuffer[m]);
	// 	glDeleteBuffers(1, &vertices[m]);
	// }

	// glDeleteBuffers(1, &vertexbuffer[0]);
	// glDeleteBuffers(1, &uvbuffer[0]);
	// glDeleteBuffers(1, &vertices[0]);
	glDeleteProgram(programID);

}



bool loadAssImp(
	const char * path,
	std::vector<unsigned short> & indices,
	std::vector<float> & vertices,
	std::vector<float> & uvs,
	std::vector<float> & normals
){
	vertices.clear();
	uvs.clear();
	normals.clear();

	Assimp::Importer importer;

	const aiScene* scene = importer.ReadFile(path, 0/*aiProcess_JoinIdenticalVertices | aiProcess_SortByPType*/);
	if( !scene) {
		fprintf( stderr, importer.GetErrorString());
		getchar();
		return false;
	}

	const aiMesh* mesh = scene->mMeshes[0]; // In this simple example code we always use the 1rst mesh (in OBJ files there is often only one anyway)

	// Fill vertices positions
	//vertices.reserve(mesh->mNumVertices);
	for(unsigned int i=0; i<mesh->mNumVertices; i++){
		aiVector3D pos = mesh->mVertices[i];
		vertices.push_back(pos.x);
		vertices.push_back(pos.y);
		vertices.push_back(pos.z);
	}

	// // Fill vertices texture coordinates
	// uvs.reserve(mesh->mNumVertices);
	// for(unsigned int i=0; i<mesh->mNumVertices; i++){
	// 	aiVector3D UVW = mesh->mTextureCoords[0][i]; // Assume only 1 set of UV coords; AssImp supports 8 UV sets.
	// 	uvs.push_back(glm::vec2(UVW.x, UVW.y));
	// }

	// Fill vertices normals
	//normals.reserve(mesh->mNumVertices);
	for(unsigned int i=0; i<mesh->mNumVertices; i++){
		aiVector3D n = mesh->mNormals[i];
		normals.push_back(n.x);
		normals.push_back(n.y);
		normals.push_back(n.z);
	}


	// Fill face indices
	//indices.reserve(3*mesh->mNumFaces);
	for (unsigned int i=0; i<mesh->mNumFaces; i++){
		// Assume the model has only triangles.
		indices.push_back(mesh->mFaces[i].mIndices[0]);
		indices.push_back(mesh->mFaces[i].mIndices[1]);
		indices.push_back(mesh->mFaces[i].mIndices[2]);
	}

	// The "scene" pointer will be deleted automatically by "importer"
	return true;
}
