#ifndef TDSCN_H_INCLUDED
#define TDSCN_H_INCLUDED

//Tridimensional objects

#include "../SUBM/glad/glad.h"//Glad
#include <GLFW/glfw3.h>

#include "../SUBM/MATH/C++/MTR.h"
#include "../SUBM/FHL/FHL.hpp"

#include <../SUBM/glm/glm/glm.hpp>
#include <../SUBM/glm/glm/gtc/matrix_transform.hpp>

#include "SHLD.h"
#include "CAM.h"
#include "TDOBJ.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>

//https://uysalaltas.github.io/2022/01/09/OpenGL_Imgui.html
class FBO{
public:
    FBO();
	// FBO(float width, float height);
 //    FBO& operator=(const FBO& fbo);//Igualador de clase
	~FBO();
    void INIT(float width, float height);//Initialize FBO
	unsigned int GFBT();//Get frame buffer texture
	void RFB(float width, float height);//Reescale frame buffer
	void Bind() const;
	void Unbind() const;
private:
	unsigned int fbo=0;
	unsigned int texture;
	unsigned int rbo;
};

class TDSCN{//Tridimensional scene
public:
    TDSCN();

    std::vector<VCLUD> CLOUDMODELS;
    std::vector<VSHEL> SHELLMODELS;

    std::vector<CAM> CAMERAS;
    int ACAM=0;//Active camera id

    glm::mat4 MVP;

    void INIT();//Draw scene
    void DRW();//Draw scene
    void DEL();//Draw scene

};




#endif
