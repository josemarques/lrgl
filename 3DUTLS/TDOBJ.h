#ifndef TDOBJ_H_INCLUDED
#define TDOBJ_H_INCLUDED

//Tridimensional objects

#include "../SUBM/glad/glad.h"//Glad
#include <GLFW/glfw3.h>

#include "../SUBM/MATH/C++/MTR.h"
#include "../SUBM/FHL/FHL.hpp"

#include <../SUBM/glm/glm/glm.hpp>
#include <../SUBM/glm/glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>

#include <assimp/Importer.hpp>      // C++ importer interface
#include <assimp/scene.h>           // Output data structure
#include <assimp/postprocess.h>     // Post processing flags

struct SFV{//Vector field vertex
    double x,y,z;//Vertex position
    std::vector<double> v;//Vector property
} ;

class VCLUD{//Vertex cloud
public:
//https://stackoverflow.com/questions/44687061/how-to-draw-points-efficiently
	float px,py,pz;//Model position
	float rx,ry,rz;//Model roation
	float sx,sy,sz;//Model scale
	//float mM[16];//Model matrix
	//void CMM();//Compute model matrix
	glm::mat4 mM;//Model matrix

	unsigned int VertexArrayID;
	unsigned int programID;

	unsigned int MatrixID;
	unsigned int ViewMatrixID;
	unsigned int ModelMatrixID;

	unsigned int vertexbuffer;
	unsigned int colorbuffer;

	std::vector<float> VP;//Vertex positions
	std::vector<float> VC;//Vertex colors

	bool RENDR=true;//Render flag

	void INIT();
    void MOD();//Modify contents
    uint nzons=8;
	float perun=1;
	void DRW();
	void DEL();

};

class VSHEL{//Vertex shell
public:
    float px,py,pz;//Model position
    float rx,ry,rz;//Model roation
    float sx,sy,sz;//Model scale
    //float mM[16];//Model matrix
    //void CMM();//Compute model matrix
    glm::mat4 mM;//Model matrix

    unsigned int VertexArrayID;
    unsigned int programID;

    unsigned int MatrixID;
    unsigned int ViewMatrixID;
    unsigned int ModelMatrixID;

    std::vector<unsigned int> vertexbuffer;
    std::vector<unsigned int> colorbuffer;
    std::vector<unsigned int> uvbuffer;
    std::vector<unsigned int> normalbuffer;

    unsigned int NumMesh=0;//Number of meshes in object

    //Matrials. if not texture TODO
    float Kdr=0.033105, Kdg=0.318547, Kdb=0.132868, Kda=1;
    float Ksr=0.500000, Ksg=0.500000, Ksb=0.500000, Ksa=1;

    std::vector<std::vector<unsigned short>> indices;
    std::vector<std::vector<float>> vertices;
	std::vector<std::vector<float>> uvs;
	std::vector<std::vector<float>> normals; // Won't be used at the moment.

	void LDGM(std::string modelpath, std::string materialpath);//Load geometry

    bool RENDR=true;//Render flag

    void INIT();
    void DRW();
    void DEL();

};


bool loadAssImp(
	const char * path,
	std::vector<unsigned short> & indices,
	std::vector<float> & vertices,
	std::vector<float> & uvs,
	std::vector<float> & normals
);

#endif
